#!/usr/bin/python3

import os
import sys
import shutil
import argparse
import toml
import json 
import getpass
import subprocess
import logging
from distutils.dir_util import copy_tree
from collections.abc import Mapping

###############################################################################
# General Format
###############################################################################

'''
[myfirsttarget]
[mysecondtarget]
[mytarget]
dependencies = [
    "myfirsttarget", "mysecondtarget"
]

config_files.backup_dir = "/data/backup/{USERNAME}/dotconfigs"
config_files.restore_dir = "~/"
config_files.paths = [
    ".vim"
]

directories = [
    {path='/my/path', symlink='/my/symlink/path', owner='myowner'}
]

repositories.ubuntu= [
    {url = "https://myurl", gpg_url="https://mygpgurl", suites="suite1", component="component", app="myapp"}
]
repositories.fedora = [
    {url = "https://myurl"}
]

packages.ubuntu = [
    "mypackage1",
    "mypackage2"
]
packages.fedora= [
    "mypackage1",
    "mypackage2"
]

code_repos = [
    {vcs="git", url="https://github.com/reponame/", path="~/dev/reponame"}
]

script.interpreter = "bash"
script.script = """
    echo 'hello-world'
"""

'''



###############################################################################
# Globals / System Information
###############################################################################

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s %(filename)s:%(lineno)d] %(msg)s",
    handlers=[
        logging.FileHandler("new_install.log", mode="w"),
        logging.StreamHandler()
    ]
)

os_release = subprocess.check_output(["cat", "/etc/os-release"]).decode("utf-8")
os_release_info = {}
for line in os_release.splitlines():
    try:
        key, value = line.split("=")
        os_release_info[key.strip()] = value.strip()
    except Exception as e:
        continue
os_id = os_release_info["ID"]
os_version_id = os_release_info["VERSION_ID"]

# Currently Logged In Username. Used primarily in conjunction with the magic
# {USERNAME} field in paths
username = getpass.getuser()
hostname = os.environ["HOSTNAME"]



###############################################################################
# Common Utils 
###############################################################################

PATH_USERNAME = "{USERNAME}"
PATH_HOSTNAME = "{HOSTNAME}"

# Fully expands a given path (e.g. replaces '~' with '/home/{user}')
def ExpandedPath(cur_path):
    cur_path = cur_path.replace(PATH_USERNAME, username)
    cur_path = cur_path.replace(PATH_HOSTNAME, hostname)
    exp_path = os.path.expanduser(cur_path)

    return exp_path


def exc_shell(command, stdin=None):
    """Wraps subprocess.Popen for logging convenience.
    stdin is passed in one time and is intended for passing a script to an 
    interpreter without generating a script file. 

    Returns (stdout, stderr, returncode)

    stdout and stderr are decoded to utf-8
    """

    logging.info(command)

    proc = subprocess.Popen(
        command, 
        shell=True, 
        stdin=subprocess.PIPE, 
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    if isinstance(stdin, str):
        stdin = stdin.encode()

    stdout, stderr = proc.communicate(stdin)
    stdout = stdout.decode("utf-8")
    stderr = stderr.decode("utf-8")

    if stdout != "":
        logging.info(stdout)
    if stderr != "":
        logging.error(f'{stderr}')

    return stdout, stderr, proc.returncode


###############################################################################
# Directory Configuration
###############################################################################
# TODO: Support Ignored directories... maybe
ignored_subpaths = [
    ".ssh"
]

class DirectoryConfig():
    def __init__(self, dir_config):
        self.path = ExpandedPath(dir_config["path"])
        if os.path.isfile(self.path):
            raise ValueError(f"Expected {self.path} to be a directory or undefined, insted got type of File.")

        self.symlink_path = dir_config.get("symlink", None)
        if self.symlink_path is not None:
            self.symlink_path = ExpandedPath(self.symlink_path)

    def setup(self):
        # Create
        if not os.path.isdir(self.path) and not os.path.isfile(self.path):
            try:
                os.makedirs(self.path)
            except PermissionError as e:
                exc_shell(f"sudo mkdir -p {self.path}")
                exc_shell(f"sudo chown {username}:{username} {self.path}")

        # Symlink
        if self.symlink_path:
            if os.path.isfile(self.symlink_path):
                raise ValueError(f"Attempting to symlink a directory to an existing file {self.symlink_path}")
            if os.path.isdir(self.symlink_path) and not os.path.islink(self.symlink_path):
                # Raises OSError when directory not empty, we want to fail here
                os.rmdir(self.symlink_path)
            # Break the symlink if it already exists and points to what we want
            if os.path.exists(self.symlink_path) and os.path.realpath(self.symlink_path) == self.path:
                os.remove(self.symlink_path)
            os.symlink(self.path, self.symlink_path)


    def backup(self):
        pass

    def restore(self):
        pass


class DirectoryConfigs():
    def __init__(self, dir_configs):
        self.dir_configs = [DirectoryConfig(dir_config) for dir_config in dir_configs]

    def backup(self):
        pass

    def restore(self):
        pass

    def setup(self):
        for dir_config in self.dir_configs:
            dir_config.setup()


###############################################################################
# Package Repository Setup:
###############################################################################

repo_install_command = {
    # For the structure of the sources.list.d/foo.list format see this link:
    #     https://manpages.ubuntu.com/manpages/xenial/man5/sources.list.5.html
    "ubuntu": "wget -O {gpg_url} | sudo apt-key add - && echo 'deb {url} {suites} {components}' | sudo tee /etc/apt/sources.list.d/{app}.list",
    "fedora": "sudo dnf install -y {url}"
}

class PackageRepos():
    def __init__(self, pkg_repo_config, os_id):
        if isinstance(pkg_repo_config, Mapping):
            pkg_repo_config = pkg_repo_config.get(os_id, [])
        self.cmds = [
            repo_install_command[os_id].format(**repo_config) for repo_config in pkg_repo_config
        ]

    def backup(self):
        pass

    def restore(self):
        pass

    def setup(self):
        for cmd in self.cmds:
            exc_shell(cmd)

###############################################################################
# Package Installation Setup:
###############################################################################


package_install_command = {
    # Both commands 
    "ubuntu": "sudo apt-get install -y --ignore-missing {pkg_list}",
    "fedora": "sudo dnf install -y {pkg_list}"
}

class PackageInstalls():
    def __init__(self, pkg_list, os_id=os_id):
        if isinstance(pkg_list, Mapping):
            pkg_list = pkg_list.get(os_id, [])

        self.cmd = ""
        if len(pkg_list) > 0:
            self.cmd = package_install_command[os_id].format(pkg_list=" ".join(pkg_list))

    def backup(self):
        pass

    def restore(self):
        pass

    def setup(self):
        stdout, stderr, returncode = exc_shell(self.cmd)
        if returncode != 0:
            raise subprocess.CalledProcessError(cmd=self.cmd, output=stdout, stderr=stderr, returncode=returncode)


###############################################################################
# Configuration Files
###############################################################################
class ConfigFiles():
    def __init__(self, config_files_config):
        self.backup_dir = ExpandedPath(config_files_config["backup_dir"])
        self.restore_dir = ExpandedPath(config_files_config["restore_dir"])
        self.files = [ExpandedPath(p) for p in config_files_config["paths"]]

    def copy(self, from_dir, to_dir):
        for f in self.files:
            src=os.path.join(from_dir, f)
            dst=os.path.join(to_dir, f)
            if os.path.isdir(src):
                copy_tree(src, dst)
            else:
                try:
                    os.makedirs(os.path.split(dst)[0], exist_ok=True)
                    shutil.copy2(src=src, dst=dst)
                except FileNotFoundError as e:
                    print(e)
                    continue

    def backup(self):
        self.copy(self.restore_dir, self.backup_dir)

    def restore(self):
        self.copy(self.backup_dir, self.restore_dir)

    def setup(self):
        return self.restore()


###############################################################################
# Code Repo Setup
###############################################################################

vcs_clone_commands = {
    "git": "git clone {url} {path}"
}

vcs_validate_commands = {
    "git": "git -C {path} rev-parse"
}

class CodeRepo():
    def __init__(self, code_repo_config):
        self.vcs = code_repo_config["vcs"]
        self.url = code_repo_config["url"]
        self.path = ExpandedPath(code_repo_config["path"])
        self.clone_cmd = vcs_clone_commands[self.vcs].format(url=self.url, path=self.path)
        self.validate_cmd = vcs_validate_commands[self.vcs].format(url=self.url, path=self.path)

    def backup(self):
        pass

    def restore(self):
        pass

    def setup(self):
        stdout, stderr, returncode = exc_shell(self.validate_cmd)
        if returncode != 0:
            stdout, stderr, returncode = exc_shell(self.clone_cmd)
        else:
            logging.info("Repo already cloned, skipping")

class CodeRepos():
    def __init__(self, code_repos_config):
        self.code_repos = [CodeRepo(cr) for cr in code_repos_config]

    def backup(self):
        for cr in self.code_repos:
            cr.backup()

    def restore(self):
        for cr in self.code_repos:
            cr.restore()

    def setup(self):
        for cr in self.code_repos:
            cr.setup()


###############################################################################
# Script Setup
###############################################################################

# Scripts are blindly executed
# Only script interpreters that accept script from stdin are supported
script_interpreters = {
    "bash": "bash -s",
    "python": "python",
    "python3": "python3"
}


class ScriptConfig():
    def __init__(self, script_config):
        self.script = script_config["script"]
        self.cmd = script_interpreters[script_config["script_interpreter"]]

    def backup(self):
        pass

    def restore(self):
        pass

    def setup(self):
        stdout, stderr, returncode = exc_shell(self.cmd, self.script)


class SetupTarget():
    def __init__(self, target_config, os_id):
        dependencies = target_config.get("dependencies", [])
        directories = target_config.get("directories", [])
        repositories = target_config.get("repositories", [])
        packages = target_config.get("packages", [])
        config_files = target_config.get("config_files", None)
        code_repos = target_config.get("code_repos", [])
        script = target_config.get("script_config", None)
   
        self.dependencies = dependencies
        self.directories = DirectoryConfigs(directories)
        self.repositories = PackageRepos(repositories, os_id)
        self.packages = PackageInstalls(packages, os_id)
        self.config_files = None if config_files is None else ConfigFiles(config_files)
        self.code_repos = CodeRepos(code_repos)
        self.script = None if script is None else ScriptConfig(script)


    def _action(self, action):
        execute_order = [
            "directories", "repositories", "packages", "config_files", "code_repos", "script"
        ]
        for k in execute_order: 
            v = getattr(self, k)
            if v is not None:
                if action == "backup":
                    v.backup()
                elif action == "restore":
                    v.restore()
                elif action == "setup":
                    v.setup()
                else:
                    pass

    def backup(self):
        self._action("backup")

    def restore(self):
        self._action("restore")

    def setup(self):
        self._action("setup")


class SetupConfig():
    def __init__(self, config_file, targets, action, os_id=os_id):
        self.raw_config = {} 
        with open(config_file, mode="r") as configf:
            self.raw_config = toml.load(configf)
        
        logging.info(f"\n{json.dumps(self.raw_config, indent=4)}")

        self.config = {}
        for k,v in self.raw_config.items():
            self.config[k] = SetupTarget(v, os_id)

        if targets == ["all"]:
            targets = sorted(self.config.keys())
            self.targets = targets
        self.action = action

        # Resolve Targets
        self.full_target_list = self.resolve_targets(targets)
        
        
        logging.info(f"Installing targets in the following order:\n{json.dumps(self.full_target_list, indent=4)}")


    def execute(self):
        proceed = self.request_user_validation()
        if proceed:
            for target in self.full_target_list:
                logging.info("="*80)
                logging.info(f"=  [{target}] - {self.action}")
                logging.info("="*80)
                setup_target = self.config[target]
                if self.action == "setup":
                    setup_target.setup()
                elif self.action == "backup":
                    setup_target.backup()
                elif self.action == "restore":
                    setup_target.restore()


    def request_user_validation(self):
        user_choice = ""
        while user_choice.lower() not in ["yes", "no"]:
            user_choice = input(f"WARNING: You will '{self.action}' your config files, do you want to proceed? Type (yes or no): ")

        return user_choice.lower() == "yes"


    def resolve_targets(self, targets, visited_targets=None):
       
        resolved_targets = []

        if visited_targets is None:
            visited_targets = set()

        for target in targets:
            if target in visited_targets:
                raise ValueError(f"{target} forms circular dependency with {visited_targets}")

            visited_targets.add(target)
            cur_resolved_targets = self.resolve_targets(
                self.config[target].dependencies,
                visited_targets
            )
            cur_resolved_targets = [t for t in cur_resolved_targets if t not in resolved_targets]
            resolved_targets += cur_resolved_targets
            visited_targets.remove(target)
            if target not in resolved_targets:
                resolved_targets.append(target)
       
        return resolved_targets
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-c", "--config", required=True, dest="config_file", help="Path to config file")
    parser.add_argument("-t", "--targets", nargs="+", required=True, dest="targets", help="Which targets to act on")
    parser.add_argument("-a", "--action", required=True, choices=["backup", "restore", "setup"], dest="action", help="Which action to take on the specified targets")
    parser.add_argument("--os-id", dest="os_id", default=os_id, help="Override OS detection")

    args = parser.parse_args()

    SetupConfig(**vars(args)).execute()
    

